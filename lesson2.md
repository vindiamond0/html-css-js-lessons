# Lesson 2

## Task1

You have to create a page. The page must content one black heading, several paragraphs with three different colors. Under second paragraph you have to insert a green link to task from lesson 1. Last paragraph must be marked with yellow background. Also insert horizontal line between second and third paragraphs.
At the bottom of the page you have to insert the link to codepen.io with your homework.

Task has been completed by: [Xenia](https://codepen.io/rcif), [Ramin](https://codepen.io/dikiy), [Dima](https://codepen.io/Dimasz)

### 1 Dima (complete 90/100) 

<p data-height="265" data-theme-id="light" data-slug-hash="gdVdmL" data-default-tab="html,result" data-user="Dimasz" data-pen-title="gdVdmL" class="codepen">See the Pen <a href="https://codepen.io/Dimasz/pen/gdVdmL/">gdVdmL</a> by Dima (<a href="https://codepen.io/Dimasz">@Dimasz</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

### 2 Xenia (complete 100/100)

<p data-height="265" data-theme-id="light" data-slug-hash="zmOpXV" data-default-tab="html,result" data-user="rcif" data-pen-title="lesson2" class="codepen">See the Pen <a href="https://codepen.io/rcif/pen/zmOpXV/">lesson2</a> by Ksenia (<a href="https://codepen.io/rcif">@rcif</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

### 3 Ramin (complete 100/100)

<p data-height="265" data-theme-id="light" data-slug-hash="oavaRG" data-default-tab="html,result" data-user="dikiy" data-pen-title="lesson█" class="codepen">See the Pen <a href="https://codepen.io/dikiy/pen/oavaRG/">lesson█</a> by Ramin (<a href="https://codepen.io/dikiy">@dikiy</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

### 4 Kirill (complete 90/100)

<p data-height="265" data-theme-id="light" data-slug-hash="GYKVZg" data-default-tab="html,result" data-user="Sally_Kromsaly" data-pen-title="GYKVZg" class="codepen">See the Pen <a href="https://codepen.io/Sally_Kromsaly/pen/GYKVZg/">GYKVZg</a> by Kiril Kiriluyk (<a href="https://codepen.io/Sally_Kromsaly">@Sally_Kromsaly</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

*To fix:*
- no closing \<html> and \<body> tags.
