# HTML CSS JS lesson 1

## Articles to learn
- [HTML Basic](https://www.w3schools.com/html/html_basic.asp)
- [HTML Elements](https://www.w3schools.com/html/html_elements.asp)
- [HTML Attributes](https://www.w3schools.com/html/html_attributes.asp)

### [Home task](https://gitlab.com/vindiamond0/html-css-js-lessons/blob/master/lesson1.md)

# HTML CSS JS lesson 2

## Articles to learn
- [HTML Headings](https://www.w3schools.com/html/html_headings.asp)
- [HTML Paragraphs](https://www.w3schools.com/html/html_paragraphs.asp)
- [HTML Styles](https://www.w3schools.com/html/html_styles.asp)

### [Home task](https://gitlab.com/vindiamond0/html-css-js-lessons/blob/master/lesson2.md)

# HTML CSS JS lesson 3

## Articles to learn
- [HTML Formatting](https://www.w3schools.com/html/html_formatting.asp)
- [HTML CSS](https://www.w3schools.com/html/html_css.asp)
- [HTML Links](https://www.w3schools.com/html/html_links.asp)

### [Home task](https://gitlab.com/vindiamond0/html-css-js-lessons/blob/master/lesson3.md)
