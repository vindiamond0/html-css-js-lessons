# Lesson 1

## Task1

You have to create a page. The page must content one second level heading with a text: "My agenda" and two paragraphs. First paragraph should content the text which describes a purpose of the agenda. Second one must be nested with a list of 5 items. Example of items: "Buy a laptop", "Read a book" etc. At the bottom of the page you have to insert the link to codepen.io with your homework.

Task has been completed by: [Xenia](https://codepen.io/rcif)

### 1 Ramin (complete 80/100)
<p data-height="265" data-theme-id="light" data-slug-hash="mGGxev" data-default-tab="html,result" data-user="dikiy" data-pen-title="My agenda" class="codepen">See the Pen <a href="https://codepen.io/dikiy/pen/mGGxev/">My agenda</a> by Ramin (<a href="https://codepen.io/dikiy">@dikiy</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

*To fix:*
- Insert declaration of HTML5 and html tag;
- Does not match the "Second one must be nested with a list of 5 items";
- Not implemented "At the bottom of the page you have to insert the link to codepen.io with your homework".

### 2 Dima (complete 70/100)
<p data-height="265" data-theme-id="light" data-slug-hash="LJaONV" data-default-tab="html,result" data-user="Dimasz" data-pen-title="LJaONV" class="codepen">See the Pen <a href="https://codepen.io/Dimasz/pen/LJaONV/">LJaONV</a> by Dima (<a href="https://codepen.io/Dimasz">@Dimasz</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

*To fix:*
- Does not match the "... and two paragraphs";
- Fix the list tag syntax; 
- Does not match the "Second one must be nested with a list of 5 items";
- Not implemented "At the bottom of the page you have to insert the link to codepen.io with your homework".

### 3 Xenia (complete 100/100)
<p data-height="265" data-theme-id="light" data-slug-hash="wExrXV" data-default-tab="html,result" data-user="rcif" data-pen-title="Lesson1" class="codepen">See the Pen <a href="https://codepen.io/rcif/pen/wExrXV/">Lesson1</a> by Ksenia (<a href="https://codepen.io/rcif">@rcif</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

### 4 Kirill (completed 70/100)
<p data-height="265" data-theme-id="light" data-slug-hash="JmPgGe" data-default-tab="html,result" data-user="Sally_Kromsaly" data-pen-title="JmPgGe" class="codepen">See the Pen <a href="https://codepen.io/Sally_Kromsaly/pen/JmPgGe/">JmPgGe</a> by Kiril Kiriluyk (<a href="https://codepen.io/Sally_Kromsaly">@Sally_Kromsaly</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

*To fix:*
- Heading shuld be a second level;
- Expected two paragraphs no one coded;

### 5 Boguslav (completed 70/100)
<p data-height="265" data-theme-id="light" data-slug-hash="OBVooJ" data-default-tab="html,result" data-user="Boguslav_Pogrebnov" data-pen-title="Д/З" class="codepen">See the Pen <a href="https://codepen.io/Boguslav_Pogrebnov/pen/OBVooJ/">Д/З</a> by Boguslav  Pogrebnov (<a href="https://codepen.io/Boguslav_Pogrebnov">@Boguslav_Pogrebnov</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

*To fix:*
- No need to embed heading by div tag;
- No second level heading represented;
- Page should contents only two paragraphs;
- No list tag represented.