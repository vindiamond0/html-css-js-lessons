Lesson 3

Task1

Design a page with three red links. Each link must become grey color when you click on it. 

Every page should be opened in a new window.

One from the links must be an image (it can be a youtube logo, so the link also should be targeted to youtube.com)

Another link must open the page https://asic.tools/ on the section named "На сервисе обслуживаются" (use a bookmark from chapter https://www.w3schools.com/html/html_links.asp)

The last link must increase font size when a mouse cursor pointed to it.

### 1 Kirill (complete /100)

<p data-height="265" data-theme-id="light" data-slug-hash="wYdePY" data-default-tab="css,result" data-user="Sally_Kromsaly" data-pen-title="wYdePY" class="codepen">See the Pen <a href="https://codepen.io/Sally_Kromsaly/pen/wYdePY/">wYdePY</a> by Kiril Kiriluyk (<a href="https://codepen.io/Sally_Kromsaly">@Sally_Kromsaly</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script> 